package com.bfustudents.bfustudents.db.repositories;

import com.bfustudents.bfustudents.db.entities.Faculty;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FacultyRepository extends JpaRepository<Faculty,Integer> {
}
