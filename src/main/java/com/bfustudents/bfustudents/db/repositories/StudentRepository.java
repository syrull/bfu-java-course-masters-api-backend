package com.bfustudents.bfustudents.db.repositories;

import com.bfustudents.bfustudents.db.entities.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student,Integer> {
}
