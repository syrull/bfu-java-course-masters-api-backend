package com.bfustudents.bfustudents.services;

import com.bfustudents.bfustudents.db.entities.Student;

import java.util.List;

public interface StudentService {
    List<Student> getAll();

    Student create(Student student);

    Student getById(Integer id);

    Student update(Integer id, Student student);
}
