package com.bfustudents.bfustudents.services;

import com.bfustudents.bfustudents.db.entities.Faculty;
import com.bfustudents.bfustudents.db.entities.StudyProgram;
import com.bfustudents.bfustudents.db.repositories.FacultyRepository;
import com.bfustudents.bfustudents.db.repositories.StudyProgramRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class StudyProgramImpl implements StudyProgramService {

    private final StudyProgramRepository studyProgramRepository;
    private final FacultyRepository facultyRepository;

    @Autowired
    public StudyProgramImpl(StudyProgramRepository studyProgramRepository, FacultyRepository facultyRepository) {
        this.studyProgramRepository = studyProgramRepository;
        this.facultyRepository = facultyRepository;
    }

    @Override
    public List<StudyProgram> getAll() {
        return studyProgramRepository.findAll();
    }

    @Override
    @Transactional
    public StudyProgram create(StudyProgram studyProgram) {
        Faculty faculty = facultyRepository.findById(studyProgram.getFaculty().getId()).orElseThrow(() -> new IllegalArgumentException("No such Faculty"));
        studyProgram.setFaculty(faculty);
        return studyProgramRepository.save(studyProgram);
    }

    @Override
    public StudyProgram getById(Integer id) {
        return studyProgramRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public StudyProgram update(Integer id, StudyProgram studyProgram) {
        StudyProgram _studyProgram = studyProgramRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("No such StudyProgram"));
        Faculty faculty = facultyRepository.findById(studyProgram.getFaculty().getId()).orElseThrow(() -> new IllegalArgumentException("No such Faculty"));

        _studyProgram.setName(studyProgram.getName());
        _studyProgram.setDegree(studyProgram.getDegree());
        _studyProgram.setTypeOfStudy(studyProgram.getTypeOfStudy());
        _studyProgram.setDescription(studyProgram.getDescription());
        _studyProgram.setFaculty(faculty);
        return studyProgramRepository.save(_studyProgram);
    }
}
