package com.bfustudents.bfustudents.controllers;

import com.bfustudents.bfustudents.db.entities.StudyProgram;
import com.bfustudents.bfustudents.services.StudyProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudyProgramController {
    private final StudyProgramService studyProgramService;

    @Autowired
    public StudyProgramController(StudyProgramService studyProgramService) {
        this.studyProgramService = studyProgramService;
    }

    @CrossOrigin
    @GetMapping("/study_programs")
    public List<StudyProgram> facultiesGet() {
        return studyProgramService.getAll();
    }

    @CrossOrigin
    @GetMapping("/study_programs/{id}")
    public StudyProgram StudyProgramGetOne(@PathVariable Integer id) {
        return studyProgramService.getById(id);
    }

    @CrossOrigin
    @PostMapping("/study_programs")
    public StudyProgram StudyProgramCreate(@RequestBody StudyProgram studyProgram) {
        return studyProgramService.create(studyProgram);
    }

    @CrossOrigin
    @PutMapping("/study_programs/{id}")
    public StudyProgram StudyProgramUpdate(@PathVariable Integer id, @RequestBody StudyProgram studyProgram) {
        return studyProgramService.update(id, studyProgram);
    }

}
